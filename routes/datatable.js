var express = require('express');
var router = express.Router();

router.get('/submit-table', function(req, res, next) {
  res.render('datatable/submit-table', { title: 'Data table submit sample' });
});

router.post('/submit-table', function(req, res, next) {
  const reqData = req.body;
  res.json(JSON.parse(reqData.q));
});

module.exports = router;
