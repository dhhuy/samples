$(document).ready(function () {

  // $('#myTable').DataTable();

  $('#add-btn').on('click', function () {
    const data = [];
    $('#myTable').find('tr').each(function (index, row) {
      if (index === 0) return;
      const temp = {
        userID: $(row).find('input[name=userID]').val(),
        note: $(row).find('input[name=note]').val(),
        check: $(row).find('input[name=check]').prop('checked')
      }
      data.push(temp);
    });
    const sendData = JSON.stringify(data);
    var newForm = jQuery('<form>', {
      'action': '/datatable/submit-table',
      'target': '_top',
      'method': 'POST'
    }).append(jQuery('<input>', {
      'name': 'q',
      'value': sendData,
      'type': 'hidden',
    }));
    $(document.body).append(newForm);
    newForm.submit();
  });

  jQuery('#fire').click(function (event) {
    event.preventDefault();
    var newForm = jQuery('<form>', {
      'action': '/',
      'target': '_top',
      'method': 'POST'
    }).append(jQuery('<input>', {
      'name': 'q',
      'value': 'stack overflow',
      'type': 'hidden'
    }));
    $(document.body).append(newForm);
    newForm.submit();
  });

});